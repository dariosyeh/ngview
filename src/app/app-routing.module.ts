import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Cinv0Component } from './cinv0/cinv0.component';
import { Sinv0Component } from './sinv0/sinv0.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/welcome' },
  { path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule) },
  { path: 'cinv0', component: Cinv0Component },
  { path: 'sinv0', component: Sinv0Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
