import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Cinv0Component } from './cinv0.component';

describe('Cinv0Component', () => {
  let component: Cinv0Component;
  let fixture: ComponentFixture<Cinv0Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Cinv0Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Cinv0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
