import { Component, OnInit } from '@angular/core';
import { invoicemain } from '../invoicetemp';
import { invoiceitem } from '../invoicetemp';

@Component({
  selector: 'app-cinv0',
  templateUrl: './cinv0.component.html',
  styleUrls: ['./cinv0.component.css']
})
export class Cinv0Component implements OnInit {
  startyear: number;
  startmonth: number;
  endyear: number;
  endmonth: number;

  startvalue?: string;
  endvalue?: string;

  invoicemain = invoicemain;
  invoiceitem = invoiceitem;

  constructor() { }

  ngOnInit(): void {
  }
  
  getvalue() {
    this.startyear = parseInt(this.startvalue.substring(0, 3)) + 1911;
    this.startmonth = parseInt(this.startvalue.substring(4, 5));
    this.endyear = parseInt(this.endvalue.substring(0, 3)) + 1911;
    this.endmonth = parseInt(this.endvalue.substring(4, 5));
    console.warn('starttime: ' + this.startyear + '/' + this.startmonth);
    console.warn('endtime: ' + this.endyear + '/' + this.endmonth);
    console.warn(this.startvalue + ',' + this.endvalue);
  }
}
