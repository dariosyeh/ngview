import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Sinv0Component } from './sinv0.component';

describe('Sinv0Component', () => {
  let component: Sinv0Component;
  let fixture: ComponentFixture<Sinv0Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Sinv0Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Sinv0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
