import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-sinv0',
  templateUrl: './sinv0.component.html',
  styleUrls: ['./sinv0.component.css']
})
export class Sinv0Component implements OnInit {
  startvalue?: string;
  endvalue?: string;

  constructor() { }

  ngOnInit(): void {
  }

  birthday = new Date(1988, 3, 15); // April 15, 1988 -- since month parameter is zero-based
  toggle = true; // start with true == shortDate

  get format()   { return this.toggle ? 'shortDate' : 'fullDate'; }
  toggleFormat() { this.toggle = !this.toggle; }

  getvalue() {
    console.warn(this.startvalue + ',' + this.endvalue);
  }
}
