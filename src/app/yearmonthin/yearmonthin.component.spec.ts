import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YearmonthinComponent } from './yearmonthin.component';

describe('YearmonthinComponent', () => {
  let component: YearmonthinComponent;
  let fixture: ComponentFixture<YearmonthinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YearmonthinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YearmonthinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
